var express = require('express');
var path = require('path');
var app = express();

const routes = require('./server/routes/routes');
const port = process.env.port || 4600;

app.use(express.static(__dirname + '/dist/bawarchi-frontend'));
app.use('/routes', routes);

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/bawarchi-frontend/index.html') )
})

app.listen(port, (req, res) => {
  console.log('running on port' + port);
})


// app.listen(8080);
//     console.log("App listening on port 8080");
