const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql2');
const port = process.env.PORT || 3000;
const app = express();

app.use(cors());
app.use(bodyparser.json());
app.use(express.static(__dirname + '/dist/bawarchi-frontend'));
app.listen(port, () => {
  console.log('server running');
});

// database connection
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'bawarchidb',
  port: 3306
  // host: 'localhost',
  // user: 'id18234225_bawarchidb',
  // password: 'Rakesh@007ab',
  // database: 'id18234225_bawarchi',
  // port: 3306
});

// check database connection
db.connect(err => {
  if (err) { console.log(err, 'err'); }
  console.log("database connected")
})

// get all data
app.get('/users', (req, res) => {
  let qr = `select * from user`;
  db.query(qr, (err, result) => {
    if (err) {
      console.log(err, 'errs');
    } else if (result.length > 0) {
      res.send({
        message: 'all users data',
        data: result
      })
    }
  })
})

// get single data
app.get('/users/:id', (req, res) => {
  let gID = req.params.id;
  let qr = `select * from user where id = ${gID}`;
  db.query(qr, (err, result) => {
    if (err) {
      console.log(err, 'errs');
    } else if (result.length > 0) {
      res.send({
        message: 'single user data',
        data: result
      })
    } else {
      res.send({
        message: 'data not found'
      })
    }
  })
})

// post data
app.post('/users', (req, res) => {
  let fullname = req.body.fullname;
  let email = req.body.email;
  let mobile = req.body.mobile;

  let qr = `insert into user(fullname, email, mobile) values('${fullname}', '${email}', '${mobile}')`;

  db.query(qr, (err, result) => {
    if (err) {
      console.log(err, 'errs');
    }
    res.send({
      message: 'data inserted'
    })
  })
})

//put data
app.put('/users/:id', (req, res) => {
  let gId = req.params.id;
  let fullname = req.body.fullname;
  let email = req.body.email;
  let mobile = req.body.mobile;

  let qr = `update user set fullname = '${fullname}', email = '${email}', mobile = '${mobile}'
                where id = ${gId}`;

  db.query(qr, (err, result) => {
    if (err) { console.log(err); }
    res.send({
      message: "data updated"
    })
  })
})

// delete data
app.delete('/users/:id', (req, res) => {
  let gId = req.params.id;
  let qr = `delete from user where id = ${gId}`;
  db.query(qr, (err, result) => {
    if (err) { console.log(err); }
    res.send({
      message: "data deleted"
    })
  })
})
