var express = require('express');
var router = express.Router();
var axios = require('axios');

const PostApi = 'https://jsonplaceholder.typicode.com'

router.get('/posts', (req,res) => {
  axios.get(`${PostApi}/posts`).then(posts =>{
    res.status(200).json(posts.data);
  }).catch(error => {
    res.status(500).send(error);
  })
})

module.exports = router;
