import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubmitRecipeRoutingModule } from './submit-recipe-routing.module';
import { SubmitRecipeComponent } from './submit-recipe.component';

import { SharedModule } from '../shared/shared.module';
import { ConfirmImageComponent } from './confirm-image/confirm-image.component';
import { ImageCropperModule } from 'ngx-image-cropper';


@NgModule({
    declarations: [
        SubmitRecipeComponent,
        ConfirmImageComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        SubmitRecipeRoutingModule,
        ImageCropperModule
    ]
})
export class SubmitRecipeModule { }
