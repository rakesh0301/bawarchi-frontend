import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { APP_URLS } from '../shared/constants/app-urls';
import { Category } from '../shared/constants/category';
import { PreparationTimeTypes } from '../shared/constants/preparationTimeTypes';
import { ConfirmImageComponent } from './confirm-image/confirm-image.component';

@Component({
  selector: 'app-submit-recipe',
  templateUrl: './submit-recipe.component.html',
  styleUrls: ['./submit-recipe.component.css']
})
export class SubmitRecipeComponent implements OnInit {
  baseImagePath = APP_URLS.baseImagePath;
  categories = Category;
  selectedFile!: File;
  reciptForm: FormGroup;
  preparationTimeTypes = PreparationTimeTypes;

  constructor(private fb: FormBuilder,
    private dialog: MatDialog,
  ) {
    this.reciptForm = this.fb.group({
      name: [null,[Validators.required, Validators.min(2)]],
      // city: ['', Validators.required],
      // emailId: ['', Validators.required],
      // category: ['', Validators.required],
      // recipeName: ['', Validators.required],
      // recipeDescription: ['', Validators.required],
      // serve: ['', Validators.required],
      // time: ['', Validators.required],
      // ingrediants: ['', Validators.required],
      // steps: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

  uploadImage() {
    const fd = new FormData();
    fd.append('file', this.selectedFile, this.selectedFile.name);
  }

  imageChangedEvent: any = '';
  croppedImage: any = '';

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.cropImage(this.imageChangedEvent);
  }

  cropImage(imageChangedEvent: any) {
    const dialogRef = this.dialog.open(ConfirmImageComponent, {
      width: '600px',
      // height: '600px',
      data: {
        message: "Do you want to upload this image?",
        imageChangedEvent: imageChangedEvent
      }
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          console.log(result);
          this.croppedImage = result;
          // this.uploadImage();
        }
      }
    );
  }
}
