import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-confirm-image',
  templateUrl: './confirm-image.component.html',
  styleUrls: ['./confirm-image.component.scss']
})
export class ConfirmImageComponent implements OnInit {

  message!: string;
  imageSrc!: string;
  error!: string;
  imageChangedEvent: any = '';

  constructor(
    public dialogRef: MatDialogRef<ConfirmImageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.message = this.data.message;
    this.imageSrc = this.data.imageSrc;
    this.imageChangedEvent = this.data.imageChangedEvent;
  }

  croppedImage: any = '';

  // fileChangeEvent(event: any): void {
  //     this.imageChangedEvent = event;
  // }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed(event: any) {
    this.error = 'Invalid image type';
    // show message

  }

  onSave() {
    this.dialogRef.close(this.croppedImage);
  }

}
