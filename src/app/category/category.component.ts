import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { APP_URLS } from '../shared/constants/app-urls';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  baseImagePath = APP_URLS.baseImagePath;

  constructor(private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  viewRecipe() {
    this.router.navigate(['', 'cat1', 'recipe'], { relativeTo: this.route, queryParamsHandling: 'preserve' });
  }
}
