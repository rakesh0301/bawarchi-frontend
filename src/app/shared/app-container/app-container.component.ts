import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { APP_URLS } from '../constants/app-urls';

@Component({
  selector: 'app-app-container',
  templateUrl: './app-container.component.html',
  styleUrls: ['./app-container.component.css']
})
export class AppContainerComponent implements OnInit {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  baseImagePath = APP_URLS.baseImagePath;

  constructor(changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private route: ActivatedRoute,
    private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      params => {
        console.log(params);
      });
  }

  goToTop() {
    this.router.navigate(['./']);
    setTimeout(() => {
      document.getElementById("topSection")?.scrollIntoView({ behavior: 'smooth' });
    }, 50);
  }

  goToAbout() {
    this.router.navigate(['./']);
    setTimeout(() => {
      document.getElementById("aboutSection")?.scrollIntoView({ behavior: 'smooth' });
    }, 50);
  }

  goToRecipe() {
    this.router.navigate(['./']);
    setTimeout(() => {
      document.getElementById("recipeSection")?.scrollIntoView({ behavior: 'smooth' });
    }, 50);
  }

}
