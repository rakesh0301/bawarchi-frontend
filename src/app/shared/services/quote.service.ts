import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuoteService {
  api_url = 'http://localhost:3000/users'
  // api_url = 'https://sweaty-forces.000webhostapp.com/users'

  constructor(private http: HttpClient) { }

  getquotes(query: string): Observable<any> {
    let headers = new HttpHeaders()
      .set('x-rapidapi-key', 'e7bf4bc82amsh1bb0689b89038a3p1fcdbcjsneed24cf50efa')
      .set('x-rapidapi-host', 'nutrition-by-api-ninjas.p.rapidapi.com');
    let url = 'https://nutrition-by-api-ninjas.p.rapidapi.com/v1/nutrition';
    let params = { query: query }
    return this.http.get<any>(url, { headers, params });
  }

  getAllPosts(): Observable<any> {
    return this.http.get('/routes/posts');
  }

  getAll(): Observable<any> {
    return this.http.get(`${this.api_url}`);
  }

}

export interface quote {

  content: string;
}
