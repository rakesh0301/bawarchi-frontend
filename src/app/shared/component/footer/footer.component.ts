import { Component, OnInit } from '@angular/core';
import { APP_URLS } from '../../constants/app-urls';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  baseImagePath = APP_URLS.baseImagePath;

  constructor() { }

  ngOnInit(): void {
  }

}
