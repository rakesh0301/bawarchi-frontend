export const NutrientsTypes = [{
  name: 'calories',
  displayname: 'Calories',
  value: 0,
  percentage: 0
}, {
  name: 'fat_total_g',
  displayname: 'Total Fat',
  value: 0,
  percentage: 0,
  weight: 'gm'
}, {
  name: 'fat_saturated_g',
  displayname: 'Saturated Fat',
  value: 0,
  percentage: 0,
  weight: 'gm'
}, {
  name: 'cholesterol_mg',
  displayname: 'Cholesterol',
  value: 0,
  percentage: 0,
  weight: 'gm'
}, {
  name: 'sodium_mg',
  displayname: 'Sodium',
  value: 0,
  percentage: 0,
  weight: 'gm'
}, {
  name: 'carbohydrates_total_g',
  displayname: 'Total Carbohydrates',
  value: 0,
  percentage: 0,
  weight: 'gm'
}, {
  name: 'fiber_g',
  displayname: 'Fiber',
  value: 0,
  percentage: 0,
  weight: 'gm'
}, {
  name: 'sugar_g',
  displayname: 'Sugar',
  value: 0,
  percentage: 0,
  weight: 'gm'
}, {
  name: 'protein_g',
  displayname: 'Protein',
  value: 0,
  percentage: 0,
  weight: 'gm'
}, {
  name: 'potassium_mg',
  displayname: 'Potassium',
  value: 0,
  percentage: 0,
  weight: 'gm'
},
]
