import { environment } from "src/environments/environment";

const cloudfrontUrl = environment.cloudfrontUrl;

export const APP_URLS = {
  baseImagePath: cloudfrontUrl + '/assets/',
}
