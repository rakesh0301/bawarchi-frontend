export const Category = [{
  name: 'Breads & Buns',
  type: 'breadsBuns'
},
{
  name: 'Biscuits & Cookies',
  type: 'biscuitsCookies'
},
{
  name: 'Cakes & Pastries',
  type: 'cakesPastries'
},
{
  name: 'Doughnuts & Crakers',
  type: 'doughnutsCrakers'
}
]
