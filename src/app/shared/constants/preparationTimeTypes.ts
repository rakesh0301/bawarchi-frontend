export const PreparationTimeTypes = [
  {
    name: 'Less than 10 min',
    type: 'less10'
  },
  {
    name: 'Upto 20 min',
    type: 'upto20'
  },
  {
    name: 'Upto 30 min',
    type: 'upto30'
  },
  {
    name: 'Upto 40 min',
    type: 'upto40'
  },
  {
    name: 'Upto 50 min',
    type: 'upto50'
  },
  {
    name: 'More than 50 min',
    type: 'more50'
  },
]
