import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NutritionValueRoutingModule } from './nutrition-value-routing.module';
import { NutritionValueComponent } from './nutrition-value.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    NutritionValueComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NutritionValueRoutingModule
  ]
})
export class NutritionValueModule { }
