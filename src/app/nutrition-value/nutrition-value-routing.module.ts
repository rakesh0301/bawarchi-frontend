import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NutritionValueComponent } from './nutrition-value.component';

const routes: Routes = [
  {
    path: '', component: NutritionValueComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NutritionValueRoutingModule { }
