import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { APP_URLS } from '../shared/constants/app-urls';
import { Chart } from 'chart.js';
import { QuoteService } from '../shared/services/quote.service';
import { Nutrition } from '../shared/constants/nutrition.model';
import { NutrientsTypes } from '../shared/constants/nutrients-types';

@Component({
  selector: 'app-nutrition-value',
  templateUrl: './nutrition-value.component.html',
  styleUrls: ['./nutrition-value.component.scss']
})
export class NutritionValueComponent implements OnInit {
  baseImagePath = APP_URLS.baseImagePath;
  @ViewChild('abilityWiseChart') abilityWiseChart: any;
  mMaintainCanvasAspectRatio = false;
  canvas1: any;
  ctx1: any;
  quote!: any;
  nutrientsTypes = NutrientsTypes;
  name!: string;
  searchRecipe!: string;
  errorMsg!: string;

  constructor(private quoteService: QuoteService) {
  }

  ngOnInit(): void {
  }

  getRecipe() {
    this.quoteService.getquotes(this.searchRecipe).subscribe(data => {
      if (data.length !== 0) {
        this.quote = data;
        this.name = this.quote[0].name;
        this.nutrientsTypes.forEach(element => {
          if (element.name == 'calories') {
            element.value = this.quote[0].calories;
          } else if (element.name == 'fat_total_g') {
            element.value = this.quote[0].fat_total_g;
            element.percentage = this.calculatePercentage(element.value, 65);
            element.weight = this.getWeigth(element.name);
          } else if (element.name == 'fat_saturated_g') {
            element.value = this.quote[0].fat_saturated_g;
            element.percentage = this.calculatePercentage(element.value, 20);
            element.weight = this.getWeigth(element.name);
          } else if (element.name == 'protein_g') {
            element.value = this.quote[0].protein_g;
            element.percentage = this.calculatePercentage(element.value, 50);
            element.weight = this.getWeigth(element.name);
          } else if (element.name == 'sodium_mg') {
            element.value = this.quote[0].sodium_mg;
            element.percentage = this.calculatePercentage(element.value, 2400);
            element.weight = this.getWeigth(element.name);
          } else if (element.name == 'potassium_mg') {
            element.value = this.quote[0].potassium_mg;
            element.percentage = this.calculatePercentage(element.value, 4700);
            element.weight = this.getWeigth(element.name);
          } else if (element.name == 'cholesterol_mg') {
            element.value = this.quote[0].cholesterol_mg;
            element.percentage = this.calculatePercentage(element.value, 300);
            element.weight = this.getWeigth(element.name);
          } else if (element.name == 'carbohydrates_total_g') {
            element.value = this.quote[0].carbohydrates_total_g;
            element.percentage = this.calculatePercentage(element.value, 300);
            element.weight = this.getWeigth(element.name);
          } else if (element.name == 'fiber_g') {
            element.value = this.quote[0].fiber_g;
            element.percentage = this.calculatePercentage(element.value, 25);
            element.weight = this.getWeigth(element.name);
          } else if (element.name == 'sugar_g') {
            element.value = this.quote[0].sugar_g;
            // element.percentage = this.calculatePercentage(element.value, 50);
            element.weight = this.getWeigth(element.name);
          }
        });
        console.log(this.nutrientsTypes);
        setTimeout(() => {
          this.getChart();
        }, 1000);
      } else {
        this.errorMsg = 'No result found for' + '' + this.searchRecipe ;
      }

    });
  }

  calculatePercentage(value: number, dv: number) {
    return parseFloat((Math.round((value / dv) * 100)).toFixed(2));
  }

  getWeigth(name: string) {
    return name.endsWith('g') ? 'gm' : 'mg'
  }

  getChart() {
    let labels: string[] = [], data: number[] = [], backgroundColor = [];

    this.nutrientsTypes.forEach((types, index) => {
      if (types.name == 'calories' || types.name == 'sugar_g') {

      } else {
        labels.push(types.displayname);
        data.push(types.percentage);
      }
    });

    this.canvas1 = this.abilityWiseChart.nativeElement;
    this.ctx1 = this.canvas1.getContext('2d');
    let abilityWiseChart = new Chart(this.ctx1, {
      type: 'horizontalBar',
      data: {
        labels: labels,
        datasets: [
          {
            // axis: 'y',
            label: '',
            data: data,
            backgroundColor: ['#ff003c', '#58508d', '#bc5090', '#ff6361', '#ffa600', '#1fe074', '#008ac5', '#788421', '#800080'],
            borderWidth: 1
          },
        ]

      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        legend: { display: false },
        title: { display: true },
        tooltips: { enabled: true },
        hover: {
          animationDuration: 0
        },
        events: [],
        scales: {
          xAxes: [{
            gridLines: { display: true },
            ticks: {
              beginAtZero: true,
              display: true,
              min: 0,
              max: 50,
              stepSize: 10
            },
            scaleLabel: {
              display: true,
              // labelString: '% of marks'
            },
          }],
          yAxes: [{
            ticks: {
              display: true,
              beginAtZero: true,
              min: 0,
              max: 100,
              stepSize: 20
            }
          }]
        },
      }
    });
  }

}
