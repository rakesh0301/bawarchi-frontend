import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { APP_URLS } from '../shared/constants/app-urls';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit {
  baseImagePath = APP_URLS.baseImagePath;

  public forms: FormGroup;

  constructor(private fb: FormBuilder,) {
    this.forms = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      comment: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

}
