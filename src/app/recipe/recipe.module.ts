import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecipeRoutingModule } from './recipe-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';

import { RecipeComponent } from './recipe.component';


@NgModule({
  declarations: [RecipeComponent],
  imports: [
    CommonModule,
    SharedModule,
    RecipeRoutingModule,
    ShareButtonsModule,
    ShareIconsModule,
  ]
})
export class RecipeModule { }
