import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppContainerComponent } from './shared/app-container/app-container.component';

const routes: Routes = [
  {
    path: '',
    component: AppContainerComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'submit-recipe',
        loadChildren: () => import('./submit-recipe/submit-recipe.module').then(m => m.SubmitRecipeModule),
      },
      {
        path: 'extras',
        loadChildren: () => import('./nutrition-value/nutrition-value.module').then(m => m.NutritionValueModule),
      },
      {
        path: ':category',
        loadChildren: () => import('./category/category.module').then(m => m.CategoryModule),
      },
      {
        path: ':category/:recipe',
        loadChildren: () => import('./recipe/recipe.module').then(m => m.RecipeModule),
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
