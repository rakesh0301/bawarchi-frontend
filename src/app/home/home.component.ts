import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { APP_URLS } from '../shared/constants/app-urls';
import { QuoteService } from '../shared/services/quote.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  baseImagePath = APP_URLS.baseImagePath;
  age: any;
  quote: string = '';

  constructor(private router: Router,
    private route: ActivatedRoute, private quoteService: QuoteService) { }

  ngOnInit(): void {
    // this.quoteService.getAllPosts().subscribe(data => {
    //   console.log(data);
    // });
    this.quoteService.getAll().subscribe(data => {
      console.log('data', data);
    });
  }

  readAll() {
    this.router.navigate(['', 'cat1']);
  }

  viewRecipe() {
    this.router.navigate(['', 'cat1', 'recipe'], { relativeTo: this.route, queryParamsHandling: 'preserve' });
  }

  calculate() {
    console.log('dsfvsdgvds')
  }

}
